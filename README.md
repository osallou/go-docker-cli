godocker CLI - Client to use godocker in command line
========================================================

Installation with pip:

    pip install godocker_cli

Update with pip:
  
    pip install godocker_cli --upgrade

Installation from sources:

    git clone https://bitbucket.org/osallou/go-docker-cli.git
    cd go-docker-cli
    python setup.py install

Update from sources (in go-docker-cli directory):

    git pull
    python setup.py install

Command-lines:

    godlogin
    goduser
    godjob
    godfile
    godimage
    godproject
    godssh
    godbatch


Documentation:

You can access to the complete godocker CLI documentation [here](https://bitbucket.org/osallou/go-docker-cli/wiki/Home)

godocker_cli is compatible with python 3
