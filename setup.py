#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup

here = os.path.abspath(os.path.dirname(__file__))
try:
    with open(os.path.join(here, 'README.md')) as f:
        README = f.read()
    with open(os.path.join(here, 'CHANGELOG')) as f:
        CHANGES = f.read()
except UnicodeDecodeError:
    with open(os.path.join(here, 'README.md'), encoding='utf-8') as f:
        README = f.read()
    with open(os.path.join(here, 'CHANGELOG'), encoding='utf-8') as f:
        CHANGES = f.read()


config = {
    'description': 'GO Docker CLI',
    'long_description': README + '\n\n' + CHANGES,
    'author': 'Olivier Sallou',
    'url': '',
    'download_url': '',
    'author_email': 'olivier.sallou@irisa.fr',
    'version': '1.0.25',
    'install_requires': ['nose',
        'pyCLI>=2.0.3',
        'terminaltables>=1.0.2',
        'click',
        'requests>=2.7.0',
        'humanfriendly'
        ],
    'packages': find_packages(),
    'include_package_data': True,
    #'scripts': ['bin/godbatch.py',  'bin/godfile.py',  'bin/godimage.py',  'bin/godjob.py',  'bin/godlogin.py',  'bin/godproject.py',  'bin/godssh.py',	'bin/goduser.py'],
    'name': 'godocker_CLI',
    'entry_points' : {
        'console_scripts': [
            'godjob = bin.godjob:run',
            'godbatch = bin.godbatch:run',
            'godlogin = bin.godlogin:login',
            'goduser = bin.goduser:run',
            'godfile = bin.godfile:run',
            'godimage = bin.godimage:run',
            'godssh = bin.godssh:run',
            'godproject = bin.godproject:run',
        ],
    },
}

setup(**config)
